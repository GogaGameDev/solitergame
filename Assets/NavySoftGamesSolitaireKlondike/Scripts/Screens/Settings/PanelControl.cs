﻿using UnityEngine;
using UnityEngine.UI;
public class PanelControl : MonoBehaviour
{
	[SerializeField]
	private GameObject checkMarkPanelObj;
	[SerializeField]
	private GameObject checkMarkObj;
	[SerializeField]
	private Image buttonImage;
	[SerializeField]
	private AspectRatioFitter buttonAspectRatio;
	[SerializeField]
	private Button button;
	[SerializeField]
	private GameObject LockScreen;
	string panelControlID = "";
	bool isLocked 
	{
		get 
		{
#if UNITY_EDITOR
			if (Loader.OpenAll) 
			{
				return false;
			}
#endif

				return !DataProvider.IsSpriteOpened(panelControlID);
		
		}
	}
	UnityEngine.Events.UnityAction clickAction = null;
	[SerializeField] ImageType _imageType;
	int _index = -1;
	public void InitButton(ImageType type, int index, Sprite sprite, UnityEngine.Events.UnityAction action)
	{
		buttonImage.sprite = sprite;
		Rect spriteRect = sprite.rect;
		//buttonAspectRatio.aspectRatio = spriteRect.width / spriteRect.height;
		//button.onClick.AddListener (action);
		button.onClick.AddListener(ClickAction);
		clickAction = action;
		// buttonImage.transform.localScale = new Vector3(1.3f, 1.3f, 1.3f);
		panelControlID = sprite.name;
		_imageType = type;
		_index = index;
		

		if (isLocked)
		{
			buttonImage.color = ImageSettings.Instance.lockColor;
			LockScreen.gameObject.SetActive(true);
		}

	}
	public void SetActive(bool isActive)
	{
		checkMarkPanelObj.SetActive(isActive);
		checkMarkObj.SetActive(isActive);

		if (isActive && LockScreen.activeSelf)
		{
			buttonImage.color = Color.white;
			LockScreen.gameObject.SetActive(false);
		}

	}
	void ClickAction()
	{

#if UNITY_EDITOR
		DataProvider.OpenSpriteToUse(panelControlID,_imageType,_index);
		clickAction?.Invoke();
		return;
#endif
		if (isLocked)
		{
			IronSourceManager.instance.ShowRewardedVideo(() =>
			{
				DataProvider.OpenSpriteToUse(panelControlID, _imageType, _index);
				clickAction?.Invoke();
			});
			return;
		}
		clickAction?.Invoke();

	}

}
public enum ImageType
{
	BackGround,
	CardFace,
	CardBack
}