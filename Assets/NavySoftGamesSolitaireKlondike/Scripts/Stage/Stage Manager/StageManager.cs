﻿//order number last 126306
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using SolitaireEngine;
using SolitaireEngine.Model;
using ManagerUtils;
using Calendar;
using System.Collections;
using UnityEngine.Events;


public partial class StageManager : MonoBehaviour, ICardActions, IManagerBaseCommands, IHUDActions
{
	private HUDController hud;

    private static StageManager _instance;

    public static StageManager instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject stageManager = GameObject.Find("StageManager");
                // GameObject obj = Instantiate(PopUpPref);

                _instance = stageManager.GetComponent<StageManager>();
            }
            return _instance;

        }
    }


	private IManagerBaseCommands manager;
	private IViewBaseCommands viewer;

	private Solitaire solitaire;
	public Solitaire GetSolitaire {get { return solitaire;}} // Debuger Del it in release

	private CommandExecutor executor = new CommandExecutor();
	[HideInInspector]
	public CommandListUpdater commandUpdater;

	private CommandBuilder commandBuilder;
	private ManagerLogic managerLogic;
    public ManagerLogic GetManagerLogic { get { return managerLogic; } }
    private bool isBestTime = false;
     
    // TODO in future: move out into asset
    string[,] dealContract = new string [,] 
	{
		{ "1", "0", "0" ,"0", "0", "0", "0" },
		{ " ", "1", "0", "0", "0", "0", "0" },
		{ " ", " ", "1", "0", "0" ,"0", "0" },
		{ " ", " ", " ", "1", "0" ,"0", "0" },
		{ " ", " ", " ", " ", "1" ,"0", "0" },
		{ " ", " ", " ", " ", " " ,"1", "0" },
		{ " ", " ", " ", " ", " " ," ", "1" }
	};

	 

	#region EnterGame
	public void OnNewGame()
	{
		if (GameSettings.Instance.isSoundSet) Sound.Instance.Shift ();

        // unblink cards to prevent bug when manager add some cards to blink when settings will opened
        // this line could be removed when pause game will work properly
        if (viewer == null) viewer = StageView.instance;
		 viewer.UnblinkAll ();




		// TODO: re-use old cards
		SolitaireStageViewHelperClass.instance.RemoveCards ();
      //  if(ContinueModeGame.instance.LoadSuccess)
       // CardItemsDeck.instance.SetDeckImage(true);
 
		// wait one update to prevent some bugs
		StartCoroutine(InitGenegal());
	}

	public void OnRestartGame()
	{
		managerLogic.InitScoring ();
		InitHUD ();
		solitaire.Restart ();
		SolitaireStageViewHelperClass.instance.ResetView ();
		viewer.Restart (solitaire.GetStartingCardsId (), OnGameRestarted);

		if (DebugGame.instance != null) {
			DebugGame.instance.StopTesting ();
			DebugGame.instance.StartTesting (SolitaireStageViewHelperClass.instance, solitaire);
		}
	}
	void OnGameRestarted(){
	 
	}
   
    public bool GetBestTime()
    {
        return isBestTime;
    }
    
    public void CreateCommand(int id ,int near_id,int parent_id,int commandCase,bool isOpen)
	{
        ICommand command = null;
        if(commandBuilder==null)
        {
            commandBuilder = new CommandBuilder(solitaire, manager, viewer);
        }
        switch (commandCase)
        {
            case 1:
                //Shift Deck
                command = commandBuilder.CreateShiftDeckCommand(parent_id);
                break;
            case 2:
                //Turn Deck
                command = commandBuilder.CreateTurnDeckCommand();
                break;
            default:
                ICommand turnCardCommand = new TurnCardCommand( viewer, parent_id, isOpen);
                ICommand moveCardCommand = new MoveCardCommand(viewer, id, near_id, parent_id,true);
                command = new CommandsPair(moveCardCommand, turnCardCommand);
                
                break;
        }

    

        executor.Execute(command);
    }

	public void OnPause()
	{
	}
    #endregion;	
    
    private void Start()
	{
		GameSettings.Instance.isGameStarted = false;
     
        viewer = StageView.instance;
		manager = this;
		commandUpdater = CommandListUpdater.instanse;
       
        managerLogic = new ManagerLogic ();

        
    }


    IEnumerator InitGenegal()
    {
        yield return new WaitForEndOfFrame();
        InitSolitaire();
        InitHUD();
        managerLogic.InitScoring();
        InitViewer();
        commandBuilder = new CommandBuilder(solitaire, manager, viewer);


        bool isTurnImage = (GameSettings.Instance.isStandardSet || (!GameSettings.Instance.isStandardSet && managerLogic.HasDeckTurn));

        CardItemsDeck.instance.SetDeckImage(isTurnImage);



        if (DebugGame.instance != null)
        {
            DebugGame.instance.StopTesting();
            DebugGame.instance.StartTesting(SolitaireStageViewHelperClass.instance, solitaire);
        }
    }
	private void InitSolitaire()
	{
        string[] solitaireData = new string[2];

        solitaireData = GameSettings.Instance.calendarData;
         
        if (solitaireData[0] == null || solitaireData[0] == string.Empty)
        {
          
            SolutionAsset solutionAsset = new SolutionAsset(GameSettings.Instance.isOneCardSet);
            solitaireData = solutionAsset.Get();
            GameSettings.Instance.calendarData = solitaireData;
        }
       
      
        PlayerPrefAPI.Set();
		CalendarParser parser = new CalendarParser ();
		List<Card> deck = parser.DeckParse (solitaireData [0]);
		List<ContractCommand> solutionCommands = parser.SolutionCommandsParse (solitaireData [1]);
		solitaire = new Solitaire (deck, solutionCommands);
	}
	private void InitViewer()
	{	
		ConstantContainer c = solitaire.GetStackHolderId();
		viewer.Init (c.NULL_CARD, c.DECK_STACK_HOLDER, c.FOUNDATION_STACK_HOLDER, c.TABLEAU_STACK_HOLDER, solitaire.GetStartingCards (), dealContract);
	}
	private void InitHUD ()
	{
		hud = HUDController.instance;
		hud.ResetStatusBars ();
	}
	#region Game Complete Logic
	private void OnGameCompleted ()
	{
		managerLogic.isGameWin = true;
        isBestTime = ((int)managerLogic.timer < StatsSettings.Instance.shortestTime[0] || StatsSettings.Instance.shortestTime[0] == 0);
        AddScoreTimeBonus ();
		managerLogic.SetStopStatsAndSetting ();
        managerLogic.SetStatsStreak();
        managerLogic.SetStatsScoreAndTime (managerLogic.score, (int) managerLogic.timer, managerLogic.moves);

		if (GameSettings.Instance.isSoundSet) Sound.Instance.Win ();
        HUDController.instance.VisibleLayout(false);
        HUDController.instance.SetSolutionLayout(false);
        PopUpManager.Instance.ShowWin (OnCardDance);
		DataProvider.FirstGameWon();
	 
	}
	private void AddScoreTimeBonus()
	{
		const float scaler = 10000;
		int bonus = (int) (1f / managerLogic.timer * scaler);
		managerLogic.score += bonus;
		HUDController.instance.SetScore(managerLogic.score); // dependency
	}
	private void OnCardDance ()
	{
		// check setting before animate card
		if (GameSettings.Instance.isCongratsScreenSet) // show card dancing effect
		{ 	
			if (GameSettings.Instance.isSoundSet) Sound.Instance.Claps ();
			SolitaireStageViewHelperClass.instance.ShowDance (OnGameCompletedAction);
		}
		else // show win window without card dancing
			OnGameCompletedAction();
	}
	private void OnGameCompletedAction()
	{
		if (GameSettings.Instance.isSoundSet) Sound.Instance.Shift ();

		if (GameSettings.Instance.isCalendarGame)
			PopUpReturnToCalendar ();
		else
			PopUpReturnToMenu();
	}
	#endregion
	// helper
	public bool HasHint
	{
		get
		{
			return (GetHints ().Count > 0);
		}
	}

	int GetRandomHint()
	{			
		SolitaireEngine.Utility.Utils utils = new SolitaireEngine.Utility.Utils ();
		return utils.RandomElement (GetHints ()).IdFrom;		
	}
	private List<ContractCommand> GetHints ()
	{
        if (solitaire.GetHints().Count > 0)
            return solitaire.GetHints();

        // if no hinds found 
        // creaTE shake deck one cvommand list
        List<ContractCommand> list_c = new List<ContractCommand>();

        bool isDeckEmpty = solitaire.IsDeckEmpty();
        bool hasHintInDeck = solitaire.HasHintInDeck(GameSettings.Instance.isOneCardSet);


        bool hasDeckBlinkStandartSet = (!isDeckEmpty && hasHintInDeck);
        bool hasDeckBlinkVegasSet = (!isDeckEmpty && hasHintInDeck && managerLogic.HasDeckTurn);

        if (hasDeckBlinkStandartSet || hasDeckBlinkVegasSet)
        {
            int deck_id = solitaire.GetLastClosedCardInDeck();
            ContractCommand c = new ContractCommand(ContractCommand.State.ShiftDeckOnece, deck_id);
            list_c.Add(c);
            return list_c;
        }
        return list_c;
    }
		
	private IEnumerator ShowSolution ()
	{
		GeneralRestartGame (true);
        HUDController.instance.SetSolutionLayout(true);
        // TODO remove it and use callback
        // wait card dealt
        yield return new WaitForSeconds (1.6f);

		SolitaireStageViewHelperClass.instance.movementManager.SetMovingSpeed(SmoothMovementManager.Speed.Solution1x);

		List<ICommand> solution_commands = commandBuilder.ConvertContractCommandToICommand (solitaire.GetSolution(), GameSettings.Instance.isOneCardSet);
       
		// block touches
		HUDController.instance.setTrigger (false, null);

	 	commandUpdater.ExecuteList (solution_commands, null, PopUpExitSolution);

		managerLogic.isAllowAutoComplete = false; // do not allow autocomplete game in Show Solution Mode.
		managerLogic.isAllowFinish = false; // do not allow finish game in Show Solution Mode.
	}
	#region Update
	private void Update()
	{
		if (GameSettings.Instance.isGameStarted && !managerLogic.isGameWin) // beter bool like isFinished
		{
			IncreaseTimers ();
	        CheckFinihUpdate ();
        
        }

        
	}

    private void OnGUI()
    {
#if UNITY_EDITOR

      
		//if (GUILayout.Button("Win Game"))
  //      {
  //          managerLogic.isAllowFinish = false;
  //          // wait when the last card arrived 
  //          StartCoroutine(runAter(() => OnGameCompleted(), .5f));
  //      }
 #endif
    }
    private void IncreaseTimers()
	{
		float time = Time.deltaTime;
		managerLogic.timer += time;
		hud.SetTime((int)managerLogic.timer);
		if (GameSettings.Instance.isStandardSet && managerLogic.scoreIncreaseTimer.Tick (time))
			managerLogic.AddScore (managerLogic.DecreaseTimeScore);
		
		if (managerLogic.hintTimer.Tick(time))
		{
			// check setting before blink card
			if (!GameSettings.Instance.isAutoHintSet)
				return;
			if (managerLogic.isAllowBlinkHint && HasHint)
			{
				viewer.BlinkCard (GetRandomHint());
				managerLogic.isAllowBlinkHint = false;
			}
		}
    }


    public IEnumerator VisibleButtonComplete()
    {
        yield return new WaitForSeconds(.7f);
        if (!IsRuleAprooveAutoComplete())
        {
            HUDController.instance.VisibleButtonComplete(false);
            managerLogic.isAllowAutoComplete = true;
        }
    }


    public void ResetAllowAutoComplete(bool allow)
    {
        managerLogic.isAllowAutoComplete = allow;
    }
    private void CheckFinihUpdate ()
	{


		if (managerLogic.isAllowAutoComplete &&  IsRuleAprooveAutoComplete())
		{
            managerLogic.isAllowAutoComplete = false;
            // wait when the card finished moving
            StartCoroutine(runAter( ()=>PopUpAutoComplete(), .5f));
		}

      
       
		if (managerLogic.isAllowFinish && solitaire.IsComplete ())
		{
			managerLogic.isAllowFinish = false;
			// wait when the last card arrived 
			StartCoroutine(runAter( ()=>OnGameCompleted(), .5f));
		}
	}
	private IEnumerator runAter(UnityAction runnable, float seconds){
		yield return new WaitForSeconds (seconds);
		if(runnable != null)
			runnable ();
	}

    
	private bool IsRuleAprooveAutoComplete()
	{
        bool isCommunityHasAllOpenCard = solitaire.IsCommunityHasAllOpenCard();


        return isCommunityHasAllOpenCard;
    }
	#endregion

	#region Exit Game
	public void GeneralRestartGame(bool fromSolution=false)
	{
		ResetActivity ();
		GameFlowDispatcher.Instance.FromManagerToRestartGame (fromSolution);
	}
	private void GeneralNewGame()
	{
	 
		ResetActivity ();
        HUDController.instance.VisibleButtonComplete(false);

		GameFlowDispatcher.Instance.FromManagerToNewGame ();
	}
	private void GeneralBackToMenu()
	{
		ResetActivity ();
		GameFlowDispatcher.Instance.FromManagerToMenu ();
	}
	private void GeneralBackToCalendar()
	{
		ResetActivity ();
		GameSettings.Instance.isGameStarted = false;
		GameFlowDispatcher.Instance.FromManagerToCalendar ();
	}
	private void GeneralBackToSettings()
	{
//		ResetActivity ();
		GameFlowDispatcher.Instance.FromManagerToSettings ();
	}
	private void ResetActivity()
	{
		executor.Reset ();
		commandUpdater.Reset ();
		viewer.UnblinkAll ();
 
		SolitaireStageViewHelperClass.instance.ResetView ();
	}
    #endregion



    public void AddStatusGame(int score, int timer, int move)
    {
        managerLogic.SetBeginMove(move);
        managerLogic.SetBeginTimer(timer);
        managerLogic.AddScore(score);
    }
}