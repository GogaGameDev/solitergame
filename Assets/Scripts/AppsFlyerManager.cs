using AppsFlyerSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppsFlyerManager : Singleton<AppsFlyerManager>
{
    // Start is called before the first frame update
#if UNITY_ANDROID
    const string AppID = "";
#endif
#if UNITY_IOS
    const string AppID = "1590843873";
#endif
    const string devKey = "FHw4spxDgjFVntSe2LD2U7";
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        /* AppsFlyer.setDebugLog(true); */
#if UNITY_ANDROID
        AppsFlyer.initSDK(devKey, null, this);
#endif
#if UNITY_IOS
        AppsFlyer.initSDK(devKey, AppID, this);

#endif
        AppsFlyer.startSDK();
        StartCoroutine(TrackTimeMinutes(1));
        StartCoroutine(TrackTimeMinutes(5));
        StartCoroutine(TrackTimeMinutes(10));
        StartCoroutine(TrackTimeMinutes(15));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void onConversionDataSuccess(string conversionData)
    {
        AppsFlyer.AFLog("onConversionDataSuccess", conversionData);
        Dictionary<string, object> conversionDataDictionary = AppsFlyer.CallbackStringToDictionary(conversionData);
        // add deferred deeplink logic here
    }

    public void onConversionDataFail(string error)
    {
        AppsFlyer.AFLog("onConversionDataFail", error);
    }

    public void onAppOpenAttribution(string attributionData)
    {
        AppsFlyer.AFLog("onAppOpenAttribution", attributionData);
        Dictionary<string, object> attributionDataDictionary = AppsFlyer.CallbackStringToDictionary(attributionData);
        // add direct deeplink logic here
    }

    public void onAppOpenAttributionFailure(string error)
    {
        AppsFlyer.AFLog("onAppOpenAttributionFailure", error);
    }
    public static void SendEvent(string eventName, Dictionary<string,string> eventParams=null) 
    {  
        AppsFlyer.sendEvent(eventName, eventParams);
    }
    IEnumerator  TrackTimeMinutes(int time) 
    {  
        yield return new WaitForSecondsRealtime(time*60);

        DataProvider.GamePLayMinutes(time);
        Debug.Log("TrackTimeMinutes Done " + time+" minutes");
    }
   
}
