using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class DataProvider 
{
    public static bool IsSpriteOpened(string spriteIDKey) 
    {
        return PlayerPrefs.GetInt(spriteIDKey, 0)==1;
    }

    public static void OpenSpriteToUse(string spriteIDKey, ImageType type, int index) 
    {
        PlayerPrefs.SetInt(spriteIDKey, 1);
        switch (type) 
        {
            case ImageType.BackGround:
                AppsFlyerManager.SendEvent("Unlock background " + index);
                break;
            case ImageType.CardBack:
                AppsFlyerManager.SendEvent("Unlock card back " + index);
                break;
            case ImageType.CardFace:
                AppsFlyerManager.SendEvent("Unlock card face "+ index);
                break;
        }
    }
    
    public static bool IsAdsRemoved 
    {
        get 
        {
            return PlayerPrefs.GetInt("adsremoved",0) == 1;
        }
    }
    public static void RemoveAds() 
    {
        PlayerPrefs.SetInt("adsremoved",1);
    }
    public static void  UpdateGamesPlayed(int[] gamesPlayed) 
    {
        int allPlayed = 0;
        foreach (var played in gamesPlayed) 
        {
            allPlayed += played;
        }

        if (allPlayed >= 5 && PlayerPrefs.GetInt("gamePlayed5", 0) == 0) 
        {
            AppsFlyerManager.SendEvent("5 games played");
            PlayerPrefs.SetInt("gamePlayed5", 1);
        }
        if (allPlayed >= 10 && PlayerPrefs.GetInt("gamePlayed10", 0) == 0)
        {
            AppsFlyerManager.SendEvent("10 games played");
            PlayerPrefs.SetInt("gamePlayed10", 1);
        }
    }
    public static void FirstGameWon() 
    {
        if (PlayerPrefs.GetInt("firstGameWon", 0) == 1)
            return;

        PlayerPrefs.SetInt("firstGameWon",1);
        AppsFlyerManager.SendEvent("1st game won");
    }
    public static bool IsAuthorized()
    {
        return PlayerPrefs.GetInt("Authorized", 0) == 1;
    }
    public static void SaveAuthorizedStatus()
    {
        PlayerPrefs.SetInt("Authorized", 1);
    }
    public static void GamePLayMinutes(int gameplayMinutes) 
    {
        switch (gameplayMinutes) 
        {
            case 5:
                if (PlayerPrefs.GetInt("gameplayMin5", 0) == 0) 
                {
                    PlayerPrefs.SetInt("gameplayMin5",1);
                    AppsFlyerManager.SendEvent("5 minutes gameplay");
                }
                break;
            case 10:
                if (PlayerPrefs.GetInt("gameplayMin10", 0) == 0)
                {
                    PlayerPrefs.SetInt("gameplayMin10", 1);
                    AppsFlyerManager.SendEvent("10 minutes gameplay");
                }
                break;
            case 15:
                if (PlayerPrefs.GetInt("gameplayMin15", 0) == 0)
                {
                    PlayerPrefs.SetInt("gameplayMin15", 1);
                    AppsFlyerManager.SendEvent("15 minutes gameplay");
                }
                break;
        }
    }
}
