using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Button))]
public class GoToLink : MonoBehaviour
{
    [SerializeField]
    string url;
    void Start()
    {
        Button button = null;

        if (TryGetComponent<Button>(out button)) 
        {
            button.onClick.AddListener(ToLink);
        }
    }

    void ToLink() 
    {
        Application.OpenURL(url);
    }
}
