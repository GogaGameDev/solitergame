using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using UnityEngine.Purchasing;

public class IAP : Singleton<IAP>
{
	public static IAP _instance;

	[HideInInspector] public UnityEvent OnPurchase;
	[HideInInspector] public UnityEvent OnInitFail;
	[HideInInspector] public UnityEvent OnInitSucess;
	[SerializeField] bool doNotDestroy;


	//removeads
    [SerializeField] ShopProductNames RemoveAds;

    private void Awake()
    {
		Init();
		if (doNotDestroy) 
		{
			DontDestroyOnLoad(this.gameObject);
		}
    }
    public void BuyIAP(ShopProductNames product)
	{
#if UNITY_EDITOR
		Debug.Log("BuyIAP " + product);
#endif
		IAPManager.Instance.BuyProduct(product, ProductBoughtCallback);	
	}

	private void Init()
	{
      IAPManager.Instance.InitializeIAPManager(InitializeResultCallback);		
	}

	private void InitializeResultCallback(IAPOperationStatus status, string message, List<StoreProduct>
shopProducts)
	{
		Debug.Log("IAP Status" + status);
		if (status == IAPOperationStatus.Success)
		{
			OnInitSucess.Invoke();
			//IAP was successfully initialized
			//loop through all products
			Debug.Log("Product Count: " + shopProducts.Count);
			for (int i = 0; i < shopProducts.Count; i++)
			{

				Debug.Log("Product ID: " + shopProducts[i].GetStoreID());
			}

		}
		else
		{
			Debug.Log("Error occurred " + message);
			OnInitFail.Invoke();
		}
	}




	// automatically called after one product is bought
	// this is an example of product bought callback
	private void ProductBoughtCallback(IAPOperationStatus status, string message, StoreProduct
	product)
	{
		if (status == IAPOperationStatus.Success)
		{
			OnPurchase.Invoke();

			if (product.productName == RemoveAds.ToString()) 
			{
				AppsFlyerManager.SendEvent("In-app purchase to remove ads");
				DataProvider.RemoveAds();
				IronSourceManager.instance?.DestroyBanner();
			}
		}
		else
		{
			//an error occurred in the buy process, log the message for more details
			Debug.Log("Buy product failed: " + message);
		}
	}


	

	public bool IsInit()
	{
		return IAPManager.Instance.IsInitialized();
	}


	
}
