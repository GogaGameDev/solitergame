using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Loader : MonoBehaviour
{
#if UNITY_EDITOR
    [SerializeField] bool _clearStart = false;
    public static bool OpenAll=true;
#endif
  
    const string backGroundSpriteKey = "Back_0_landSpace";
    const string cardBackSpriteKey = "card_back_1";
    const string cardFaceSpriteKey = "card_face_1";

    void Start()
    {
#if UNITY_EDITOR
        if (_clearStart) 
        {
            PlayerPrefs.DeleteAll(); 
        }
#endif
        OpenFirstSprites();
    }


     void OpenFirstSprites() 
     {
       PlayerPrefs.SetInt(backGroundSpriteKey,1);
       PlayerPrefs.SetInt(cardBackSpriteKey, 1);
       PlayerPrefs.SetInt(cardFaceSpriteKey, 1);
    }

    public void LoadGame()
    {
        SceneManager.LoadScene(1);

    }

}
