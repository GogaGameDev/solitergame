using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestorePurchase : MonoBehaviour
{
    Button restorePurchaseButton;
    void Start()
    {
        restorePurchaseButton = GetComponent<Button>();
        restorePurchaseButton.onClick.AddListener(Restore);
    }

    void Restore()
    {


            if (IAPManager.Instance != null && IAPManager.Instance.IsInitialized())
            {
               IAPManager.Instance.RestorePurchases(ProductRestoredCallback);
                 Debug.Log("Restoring Purchases...");
            }
        

    }
    private void ProductRestoredCallback(IAPOperationStatus status, string message, StoreProduct
product)
    {
        if (status == IAPOperationStatus.Success)
        {
            Debug.Log(" product restored: " + product.GetStoreID());
        }
        else
        {
            //an error occurred in the buy process, log the message for more details
            Debug.Log("Buy product failed: " + message);
        }
    }

}
